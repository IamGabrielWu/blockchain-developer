pragma solidity ^0.4.20;

contract mortal{
    address owner;
    modifier onlyowner(){
        if(owner == msg.sender){
            _;
        }else{
        //what throw will do is to refund e.g. if a user sends 1000 ether to setMyVariable, and he's not allowed, then the ether will be returned back
            revert();
        }
    }
    function mortal () public{
        owner=msg.sender;
    }

    function kill() public onlyowner{
        selfdestruct(owner);
    }
}
